#!/usr/bin/python

import sys

def combine(models, sentence_pairs, validation_alignment, n_iteration=20):
    def derivative(lambda_value, current_model, additional_model):
        dL = 0
        for (source, target), alignment in \
            zip(sentence_pairs, validation_alignment):
            for t, s in alignment:
                c = current_model(t, s, source, target)
                d = additional_model(t, s, source, target)
                dL += (c - d) / (lambda_value * c + (1 - lambda_value) * d)
        return dL

    def linear_interpolation(lambda_value, current_model, additional_model):
        def calculate(*params):
            # print >>sys.stderr, params
            # print >>sys.stderr, "trace lambda=", lambda_value, 
            current_part = lambda_value * current_model(*params)
            # print >>sys.stderr, "current_part=", current_model(*params)
            additional_part = (1 - lambda_value) * additional_model(*params)
            # print >>sys.stderr, "additional_part=", additional_model(*params)
            return current_part + additional_part
        return calculate
        
    current_model = models[0]
    for additional_model in models[1:]:
        lambda_value = None
        if derivative(0.0, current_model, additional_model) <= 0:
            lambda_value = 0.0
        elif derivative(1.0, current_model, additional_model) >= 0:
            lambda_value = 1.0
        else:
            left = 0.0
            right = 1.0
            mid = None
            for iter in range(n_iteration):
                mid = (left + right) / 2
                # print "mid = ", mid
                mid_derivative = derivative(mid, current_model, \
                                            additional_model)
                # print "d=", mid_derivative
                if mid_derivative > 0:
                    left = mid
                elif mid_derivative < 0:
                    right = mid
                else:
                    break
            lambda_value = mid

        print >> sys.stderr, "lambda_value =", lambda_value
        current_model = linear_interpolation(lambda_value,
                                             current_model,
                                             additional_model)

    return current_model
