#!/usr/bin/python

from transform_model import models, rev_models
from combine import combine
from ibm12 import align
import sys
from ibm12 import model12, align, align_with_null
from transform import aligners
from extract import phrase_table, build_bi_alignment
from lm import lm
from decode import decode


debug_file = sys.stderr

align_file = open("test/notshuf/held-out.txt")
alignment_lines = align_file.readlines()[2::3]
alignments = map(eval, alignment_lines)

sources = [source.lower()[:-1].split() \
           for source in open("test/notshuf/head.en").readlines()]
targets = [target.lower()[:-1].split() \
           for target in open("test/notshuf/head.vn").readlines()]

n_eval = len(alignments)
my_model = combine(models, zip(sources[:n_eval], targets[:n_eval]), alignments)
rev_alignments = [[(s,t) for (t,s) in alignment] for alignment in alignments]
my_rev_model = combine(rev_models, zip(targets[:n_eval], sources[:n_eval]), rev_alignments)

print >> debug_file, "begin"
# sources = [source.lower()[:-1].split() \
#            for source in open(sys.argv[1]).readlines()]
# targets = [target.lower()[:-1].split() \
#            for target in open(sys.argv[2]).readlines()]

# alignments = aligners[1](zip(sources, targets))
alignments = align(zip(sources, targets), my_model)
print "finish"

print >> debug_file, "first direction done"
# rev_aligns = aligners[1](zip(targets, sources))
rev_aligns = align(zip(targets, sources), my_rev_model)
rev_aligns = [[(t, s) for (s, t) in alignment] for alignment in rev_aligns]
print >> debug_file, "second direction done"
print >> debug_file, "aligning done"

model = phrase_table()

for source, target, alignment, rev_align in zip(sources, targets, alignments, rev_aligns):
    # alignment = [(s, t) for t, s in enumerate(alignment)]
    # rev_align = [(s, t) for s, t in enumerate(rev_align)]
    # combined = build_bi_alignment(source, target, alignment, rev_align)
    # model.train(source, target, combined)

    # alignment = [(t, s) for t, s in enumerate(alignment)]
    # rev_align = [(t, s) for s, t in enumerate(rev_align)]
    combined = build_bi_alignment(target, source, alignment, rev_align)
    model.train(target, source, combined)

# model.generate_output()
table = model.get_table()

print >> debug_file, "extracting done"

lmodel = lm()
lmodel.train([sent.lower().split() for sent in open(sys.argv[2]).readlines()])

print >> debug_file, "lm done"
print >> debug_file
print >> debug_file, "What do you want translate?"

while True:
    print >> debug_file, "enter here:"
    input = raw_input()    
    print >> debug_file, "translating", input

    result = decode(input.split(), table, lmodel)
    print >> debug_file, "result: ", result
    print result
