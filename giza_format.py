#!/usr/bin/python

import re
from operator import itemgetter

def dump_giza_file(sentence_pairs, alignments, giza_file=None):    
    def giza_format(source, target, alignment):
        from_source = [[] for s in range(len(source))]
        not_aligned = range(len(target))

        for t, s in alignment:
            from_source[s].append(t)
            not_aligned.remove(t)

        def group_indices(indices):
            if len(indices) == 0:
                return "({ })"
            else:
                return "({ %s })" % (" ".join([str(i+1) for i in indices]))

        info = " ".join(target)
        info += "\n"
        info += "NULL %s" % (group_indices(not_aligned))
        for s in range(len(source)):
            in_text = source[s] + " " + group_indices(from_source[s])
            info += " " + in_text

        return info

    outputs = []

    for (source, target), alignment in zip(sentence_pairs, alignments):
        info = giza_format(source, target, alignment)
        if giza_file is None:
            outputs.append(info)
        else:
            print >> giza_file, info

    if giza_file is not None:
        return "\n".join(outputs)

def parse_giza_file(giza_file):
    def parse_giza(align_text):
        p = re.compile('(\S+)\s+\(\{(.*?)\}\)')

        parsed = p.findall(align_text)
        del parsed[0]
        
        source = map(itemgetter(0), parsed)
        aligned_to = map(itemgetter(1), parsed)
        alignments = [[(int(target) - 1, foreign) for target in targets.split()]
                           for foreign, targets in enumerate(aligned_to)]
        return source, [alignment for group in alignments for alignment in group]

    sentence_pairs = []
    alignments = []
    target = None
    for line in giza_file:
        if line.lstrip().startswith('#'):
            continue
        if target is None:
            target = line.split()
        else:
            align_text = line
            source, alignment = parse_giza(align_text)
            sentence_pairs.append((source, target))
            alignments.append(alignment)
            target = None

    return sentence_pairs, alignments

if __name__ == "__main__":
    import sys
    from ibm12 import align
    sources = [source.lower()[:-1].split() for source in open(sys.argv[1]).readlines()]
    targets = [target.lower()[:-1].split() for target in open(sys.argv[2]).readlines()]
    sentence_pairs = zip(sources, targets)
    # for source, target, alignment in zip(sources, targets, align(sentence_pairs)):
    #     print giza_format(alignment, source, target)
    dump_giza_file(sentence_pairs,
                   align(sentence_pairs),
                   open("/tmp/out", "w"))

    sen_pairs, aligns = parse_giza_file(open("/tmp/out"))
    dump_giza_file(sen_pairs, aligns, open("/tmp/out2", "w"))
