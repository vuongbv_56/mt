#!/usr/bin/python

from collections import defaultdict
import re
NULL = "<null>"

def get_alignment_model(translation_probability, distance_probability):
    def calculate(i, source, target):        
        null_source = source + [NULL]
        numerators = [translation_probability[(target[i], null_source[j])] *
                 distance_probability[(j, i, len(target), len(source))] \
                 for j in range(len(null_source))]
        total = sum(numerators)
        return [num / total for num in numerators]
    return calculate
    
def model12(sentence_pairs, n_iterations=5, second_version=True):
    if second_version:
        translation_probability = model12(sentence_pairs, n_iterations, False)[0]
    else:
        translation_probability = defaultdict(lambda: 1.0)

    distance_probability = defaultdict(lambda: 1.0)

    for iteration in range(n_iterations):
        count = {"e,f": defaultdict(float),
                 "f": defaultdict(float),
                 "j,i,le,lf": defaultdict(float),
                 "le,lf": defaultdict(int)}

        model = get_alignment_model(translation_probability,
                                    distance_probability)
        
        for source, target in sentence_pairs:
            lf = len(source)
            le = len(target)

            null_source = source + [NULL]
            for i, e in enumerate(target):
                probs = model(i, source, target)
                for j, f in enumerate(null_source):
                    evidence = probs[j]
                    count["e,f"][(e, f)] += evidence
                    count["f"][f] += evidence                    
                    if second_version:
                        count["j,i,le,lf"][(j, i, le, lf)] += evidence 

            if second_version:
                count["le,lf"][(le, lf)] += 1
            
        translation_probability = defaultdict(float)
        
        for e, f in count["e,f"].keys():
            translation_probability[(e, f)] = count["e,f"][(e, f)] / \
                                              count["f"][f]

        if second_version:
            distance_probability = defaultdict(float)
            for j, i, le, lf in count["j,i,le,lf"].keys():
                distance_probability[(j, i, le, lf)] \
                    = count["j,i,le,lf"][(j, i, le, lf)] \
                    / count["le,lf"][(le, lf)]

    return translation_probability, distance_probability

def align(sentence_pairs, model = None):
    if model is None:
        model = get_alignment_model(*model12(sentence_pairs))
    
    result = []
    for source, target in sentence_pairs:
        alignment = []
        for i in range(len(target)):
            probs = model(i, source, target)
            best_index = probs.index(max(probs))
            if best_index < len(source):
                alignment.append((i, best_index))
        result.append(alignment)
    return result

