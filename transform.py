#!/usr/bin/python

import sys
from ibm12 import model12, get_alignment_model
        

def prev_concat(sentence):
    return zip(["<s>"] + sentence[:-1], sentence)
def next_concat(sentence):
    return zip(sentence, sentence[1:] + ["</s>"])

def identity(source, target):
    return source, target

def prev_src_transform(source, target):
    return prev_concat(source), target

def prev_tgt_transform(source, target):
    return source, prev_concat(target)
def next_src_transform(source, target):
    return next_concat(source), target

def next_tgt_transform(source, target):
    return source, next_concat(target)

def prev_src_tgt_transform(source, target):
    return prev_concat(source), prev_concat(target)

def next_src_tgt_transform(source, target):
    return next_concat(source), next_concat(target)

transforms = [identity,
              prev_src_transform,
              prev_tgt_transform, 
              next_src_transform,
              next_tgt_transform,
              prev_src_tgt_transform,
              next_src_tgt_transform]

def transform_model(transform, model):
    def new_model(i, source, target):
        new_source, new_target = transform(source, target)
        return model(i, new_source, new_target)
    return new_model

def get_additional_models(sentence_pairs):
    models = []
    for trans in transforms:
        new_pairs = [trans(source, target) \
                     for source, target in sentence_pairs]
        model = get_alignment_model(*model12(new_pairs))
        models.append(transform_model(trans, model))
    return models
