#!/usr/bin/python
from collections import defaultdict

def dump_moses_table(table, output_file=None):
    def format(target, source, score):
        return source + ' ||| ' + target + ' ||| ' + str(score) + ' ||| ||| '
    
    outputs = []
    for source, entries in table.items():
        for target, score in entries.items():
            present = format(target, source, score)
            if output_file is None:
                outputs.append(present)
            else:
                print >>output_file, present

    if output_file is None:
        return "\n".join(outputs)

def parse_moses_table(input_file):
    def parse(line):
        source, target, score = line.split('|||')[:3]
        source = source.strip()
        target = target.strip()
        score = float(score)
        return source, target, score
        table = defaultdict(lambda: defaultdict(float))

    table = defaultdict(lambda: defaultdict(float))
    for line in input_file:
        source, target, score = parse(line)
        table[source][target] = score
    return table

if __name__ == "__main__":
    from ibm12 import model12
    from extract import phrase_table, build_bi_alignment
    from lm import lm
    from decode import decode
    from mt_model import build_mt_model
    import sys

    sources = [source.lower()[:-1].split()
               for source in open(sys.argv[1]).readlines()]
    targets = [target.lower()[:-1].split()
               for target in open(sys.argv[2]).readlines()]

    sentence_pairs = zip(sources, targets)

    table, lmodel = build_mt_model(sentence_pairs)

    dump_moses_table(table, open("/tmp/table1", "w"))
    table2 = parse_moses_table(open("/tmp/table1"))
    dump_moses_table(table, open("/tmp/table2", "w"))
