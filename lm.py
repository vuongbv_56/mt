#!/usr/bin/python

import sys
from collections import defaultdict
USING_BEG_END = True
SENT_BEG = "<s>"
SENT_END = "</s>"

class lm:
    def __init__(self, max_ngram = 3):
        self.count = [defaultdict(int) for _ in range(max_ngram)]
        self.count_of_count = [defaultdict(int) for _ in range(max_ngram)]
        self.max_ngram = max_ngram
        
    def train(self, sentences):
        for sentence in sentences:
            if USING_BEG_END:
                sentence = tuple([SENT_BEG]*(self.max_ngram-1) +
                                 sentence +
                                 [SENT_END])
            for n in range(self.max_ngram):
                for start in range(len(sentence) - (n+1)):
                    ngram = sentence[start:start+n+1]
                    self.count[n][ngram] += 1

        vocab_size = len(self.count[0])
        for n in range(self.max_ngram):
            for ngram, cnt in self.count[n].items():
                self.count_of_count[n][cnt] += 1
                
            self.count_of_count[n][0] = vocab_size ** (n+1) - len(self.count[n])

    def conditional_prob(self, word, prev_words):
        def refined_count(ngram):
            n = len(ngram) - 1
            r = self.count[n][ngram]
            n_r = self.count_of_count[n][r]
            n_r_1 = self.count_of_count[n][r+1]
            if n_r > 0 and n_r_1 > 0:
                return (r + 1) * float(n_r_1) / n_r
            else:
                return r

        return float(refined_count(tuple(prev_words + (word,)))) \
            / refined_count(tuple(prev_words))

    def estimate(self, sentence):
        prob = 1.0
        if USING_BEG_END:
            sentence = tuple([SENT_BEG]*(self.max_ngram-1) +
                             sentence +
                             [SENT_END])

        for start in range(self.max_ngram-1, len(sentence)):
            current = sentence[start]
            previous = sentence[start-self.max_ngram+1: start]
            
            prob *= self.conditional_prob(current, previous)
            
        return prob
if __name__ == "__main__":
    model = lm()
    model.train([sent.lower().split() for sent in open(sys.argv[1]).readlines()])
