#!/usr/bin/python

import sys
from itertools import groupby
from operator import itemgetter
from collections import defaultdict

debug_file = open("/dev/null", "w")

def reverse_table(table):
    new_table = defaultdict(lambda: defaultdict(float))
    for source in table.keys():
        for target in table[source].keys():
            new_table[target][source] = table[source][target]
    return new_table
    
def decode(source_sentence, phrase_table, language_model, reordering_model=None):
    phrase_table = reverse_table(phrase_table)
    
    MAX_STACK_SIZE = 100
    TRANSLATED = "translated"
    TEXT = "text"
    SCORE = "score"
    ESTIMATED = "estimated"
    LAST_TRANSLATED = "last_translated"
    
    source_len = len(source_sentence)
    if reordering_model is None:
        # reordering_model = [0.0 for _ in range(source_len+1)]
        reordering_model = {}
        decay = 0.5
        for dist in range(1, source_len+1):
            reordering_model[dist] = decay ** dist

    stacks = [list() for _ in range(source_len + 1)]

    init_hypo = {TRANSLATED: [False] * source_len,
                 TEXT: "",
                 SCORE: 1.0,
                 ESTIMATED: 0,
                 LAST_TRANSLATED: -1}

    stacks[0].append(init_hypo)
    
    estimated_cost = {}
    for length in range(1, source_len+1):
        for start in range(source_len-(length-1)):
            phrase = " ".join(source_sentence[start: start+length])
            cost = 1e-10 #low enough to cover brandnew words
                        
            if phrase in phrase_table:
                entries = phrase_table[phrase]
                cost = entries[max(entries)]

            end = start+length-1
            for mid in range(start, end):
                cost = max(cost, estimated_cost[(start, mid)] \
                           * estimated_cost[(mid+1,end)])
            estimated_cost[(start, end)] = cost

    def estimate_cost(hypothesis):
        cost = hypothesis[SCORE]
        
        translated = hypothesis[TRANSLATED]
        for category, group in groupby(enumerate(translated), itemgetter(1)):
            if category:
                continue
            positions = map(itemgetter(0), group)
            start = min(positions)
            end = max(positions)

            cost *= estimated_cost[(start, end)]

        hypothesis[ESTIMATED] = cost
        
    def generate(hypothesis):
        result = []
        for start in range(source_len):
            for end in range(start,source_len):
                if True not in hypothesis[TRANSLATED][start:end+1]:
                    phrase = " ".join(source_sentence[start:end+1])
                    options = []
                    if phrase in phrase_table:
                        options = phrase_table[phrase].items()
                    elif end == start: # one word case
                        options =[(phrase, 1.0)]
                        
                    for text, score in options:
                        new_hypo = {}
                        new_hypo[TRANSLATED] = hypothesis[TRANSLATED][:]
                        for i in range(start, end+1):
                            new_hypo[TRANSLATED][i] = True
                        new_hypo[TEXT] = hypothesis[TEXT] + " " + text

                        distance = abs(start - hypothesis[LAST_TRANSLATED])
                        # print >> sys.stderr, start, "-", hypothesis[LAST_TRANSLATED
                        # ], "=", distance 
                        try:
                            new_hypo[SCORE] = hypothesis[SCORE] * \
                                          score * \
                                          reordering_model[distance]
                        except Exception:
                            print >>sys.stderr, "ERR"
                            print >>sys.stderr,hypothesis
                            exit()

                        new_hypo[LAST_TRANSLATED] = end
                        result.append(new_hypo)
        return result
                            
                        
    def prune(hypo_stack):
        for hypo in hypo_stack:
            if ESTIMATED not in hypo:
                estimate_cost(hypo)
                
        worst_one = min(hypo_stack, key=itemgetter(ESTIMATED))
        hypo_stack.remove(worst_one)

    for n_words in range(source_len + 1):
        print >> debug_file, "STACK", n_words

        stack = stacks[n_words]
        for hypo in stack:
            print >> debug_file, "\t".join(map(str, hypo.values()))
        for hypo in stack:
            for new_hypo in generate(hypo):
                n_translated = new_hypo[TRANSLATED].count(True)
                stacks[n_translated].append(new_hypo)
                if len(stacks[n_translated]) > MAX_STACK_SIZE:
                    prune(stacks[n_translated])

    print >> debug_file, "enter finals round"
    finals = stacks[source_len]
    for hypo in finals:
        words = hypo[TEXT].split()
        lm_score = language_model.estimate(words)
        hypo[SCORE] *= lm_score

    for i, hypo in enumerate(finals):
        print >> debug_file, "candidate ", i
        print >> debug_file, hypo[TEXT]
        print >> debug_file, "translation prob", hypo[SCORE],\
            "lm score", lm_score

    if len(finals) == 0:
        return "NO VALID TRANSLATION"
        
        
    best_hypo = max(finals, key=itemgetter(SCORE))
    print >> debug_file, "found the best hypo"
    hypo = best_hypo
    print >> debug_file, hypo[TEXT]
    print >> debug_file, "translation prob", hypo[SCORE],\
        "lm score", lm_score
    
    return best_hypo[TEXT]
