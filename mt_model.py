#!/usr/bin/python

from ibm12 import model12, align
from extract import phrase_table, build_bi_alignment
from lm import lm
from decode import decode
import sys

debug_file = sys.stderr #open("/dev/null", "w")

# sources = [source.lower()[:-1].split() \
#            for source in open(sys.argv[1]).readlines()]
# targets = [target.lower()[:-1].split() \
#            for target in open(sys.argv[2]).readlines()]

def build_mt_model(sentence_pairs, alignment_model=None):
    alignments = align(sentence_pairs, alignment_model)
    rev_aligns = align(sentence_pairs, alignment_model)
    rev_aligns = [[(t, s) for (s, t) in alignment]
                  for alignment in rev_aligns]
    print >> debug_file, "aligning done"

    model = phrase_table()

    for (source, target), alignment, rev_align in zip(
            sentence_pairs, alignments, rev_aligns):
        combined = build_bi_alignment(target, source,
                                      alignment, rev_align)
        model.train(target, source, combined)

    table = model.get_table()

    print >> debug_file, "extracting done"

    lmodel = lm()
    lmodel.train([target for source, target in sentence_pairs])

    print >> debug_file, "lm done"
    print >> debug_file, "building mt model done"
    return table, lmodel

# table, lmodel = build_mt_model(zip(sources, targets))
# for input in sys.stdin.readlines():
#     print >> debug_file, "enter here:"
#     # input = raw_input()
#     print >> debug_file, "translating", input

#     result = decode(input.split(), table, lmodel)
#     print >> debug_file, "result: ", result
#     print result

    
