#!/usr/bin/python

from ibm12 import model12
from extract import phrase_table, build_bi_alignment
 from lm import lm
from decode import decode
from transform_model import get_additional_models\
from mt_model import build_mt_model
import sys

sources = [source.lower()[:-1].split()
           for source in open(sys.argv[1]).readlines()]
targets = [target.lower()[:-1].split()
           for target in open(sys.argv[2]).readlines()]

sentence_pairs = zip(sources, targets)

models = get_additional_models(sentence_pairs)

sentences = sys.stdin.readlines()

for i, model in enumerate(models):
    print >> sys.stderr, "Translating with model #" + str(i)
    output = open("data/output." + str(i), "w")

    table, lmodel = build_mt_model(sentence_pairs, model)
    for sentence in sentences:
        print >> sys.stderr, sentence
        result = decode(sentences.split(), table, lmodel)
        print >>output, result

    output.close()
