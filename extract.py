#!/usr/bin/python
import sys
from itertools import combinations, product
from collections import defaultdict    
from operator import itemgetter

class phrase_table:
    def __init__(self):
        self.source_count = defaultdict(int)
        self.phrase_count = defaultdict(int)

    def train(self, source_sentence, target_sentence, alignment):
        src_len = len(source_sentence)
        tgt_len = len(target_sentence)

        for src_start in range(src_len): 
          for src_end in range(src_start, src_len):
            correspondants = [tgt_ind for src_ind, tgt_ind in alignment
                              if src_ind in xrange(src_start, src_end+1)]

            if len(correspondants) == 0:
                continue
                
            minimal_start = min(correspondants)
            minimal_end = max(correspondants)
            rev_correspondants = [src_ind for src_ind, tgt_ind in alignment
                                  if tgt_ind in xrange(
                                          minimal_start, minimal_end+1)]

            if any([x not in xrange(src_start, src_end+1)
                    for x in rev_correspondants]):
                continue

            extended_start = minimal_start
            extended_end = minimal_end

            def is_aligned(tgt_ind):
                return any([(src_ind, tgt_ind) in alignment
                            for src_ind in range(src_len)])

            while not is_aligned(minimal_start-1) and \
                  minimal_start-1 in xrange(tgt_len):
                minimal_start -= 1

            while not is_aligned(minimal_end+1) and \
                  minimal_end+1 in xrange(tgt_len):
                minimal_end += 1


            for start in range(extended_start, minimal_start+1):
                for end in range(minimal_end, extended_end+1):
                    source = ' '.join(source_sentence[src_start:src_end+1])
                    target = ' '.join(target_sentence[start:end+1])
                    phrase = (target, source)

                    self.source_count[source] += 1
                    self.phrase_count[phrase] += 1

    def get_table(self):
        table = defaultdict(lambda: defaultdict(float))
        for (target, source), phrase_cnt in self.phrase_count.items():
            source_cnt = self.source_count[source]
            table[source][target] = float(phrase_cnt) / source_cnt

        return table

def build_bi_alignment(source_sentence, target_sentence,
                       alignment1, alignment2):
    """ Return the list aligned pair.
    (i, j) in the list means the i'th word of sentence1 is aligned to
    j'th word of sentence2 """

    alignment1 = set(alignment1)
    alignment2 = set(alignment2)
    src_len = len(source_sentence)
    tgt_len = len(target_sentence)
    alignment = alignment1.intersection(alignment2)
    union = alignment1.union(alignment2)

    def is_aligned_in_source(src_ind):
        return any([src_ind == pair[0] for pair in alignment])

    def is_aligned_in_target(tgt_ind):
        return any([tgt_ind == pair[1] for pair in alignment])

    neighboring_offset = ((-1,0), (0, -1), (1,0), (0,1),
                          (-1,-1), (-1,1), (1,-1), (1,1))

    considerings = union.difference(alignment)
    while True:            
        new_point_added = False
        for source_index, target_index in considerings:
            if is_aligned_in_source(source_index) \
               and is_aligned_in_target(target_index):
                continue

            for neightbor_src_ind, neighbor_tgt_ind \
                in [(source_index +x, target_index +y) \
                    for x, y in neighboring_offset]:
                if  (neightbor_src_ind, neighbor_tgt_ind) \
                    in alignment:
                    new_point_added = True
                    alignment.add((source_index, target_index))
                    break

        if not new_point_added:
            break

    for src_ind in range(src_len):
        for tgt_ind in range(tgt_len):
            if (not is_aligned_in_source(src_ind) \
                or not is_aligned_in_target(tgt_ind))\
                and (src_ind, tgt_ind) in union:
                alignment.add((src_ind, tgt_ind))

    return alignment

if __name__ == "__main__":
    from ibm12 import model12, align
    sources = [source.lower()[:-1] for source in open(sys.argv[1]).readlines()]
    targets = [target.lower()[:-1] for target in open(sys.argv[2]).readlines()]
    alignments = align(zip(sources, targets))
    rev_aligns = align(zip(targets, sources))


    model = phrase_table()

    for source, target, alignment, rev_align in zip(sources, targets, alignments, rev_aligns):
        source, target = source.split(), target.split()
        alignment = [(s, t) for t, s in enumerate(alignment)]
        rev_align = [(s, t) for s, t in enumerate(rev_align)]
        combined = build_bi_alignment(source, target, alignment, rev_align)
        model.train(source, target, combined)

    model.generate_output()
